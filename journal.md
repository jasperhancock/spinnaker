# Must login with vault
~/repo/tech-tools/scripts/k8s-auth --username <ldap username> --cluster ae.dev --role cluster-admin

You can add an alias for this in your bash_profile


# Added ae-dev secret and config

kubectl create secret generic --from-file=$HOME/.kube/config aedevconfig

Update values.yaml as such:

```
kubeConfig:
  # Use this when you want to register arbitrary clusters with Spinnaker
  # Upload your ~/kube/.config to a secret
  enabled: true
  secretName: aedevconfig
  secretKey: config
  # List of contexts from the kubeconfig to make available to Spinnaker
  contexts:
  - ae-dev
```
